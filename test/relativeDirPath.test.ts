describe('Relative path to directory alias', () => {
	const fixture = require('../fixture/relative_path_to_dir/dist/bundle');

	it('should get expected bundle', () => {
		expect(fixture).toBeDefined();
		expect(fixture).not.toBeNull();
		expect(fixture).toHaveProperty('print');
		expect(fixture.print).toBeInstanceOf(Function);
	});
});
