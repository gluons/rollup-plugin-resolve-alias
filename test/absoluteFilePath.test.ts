describe('Absolute path to file alias', () => {
	const fixture = require('../fixture/absolute_path_to_file/dist/bundle');

	it('should get expected bundle', () => {
		expect(fixture).toBeDefined();
		expect(fixture).not.toBeNull();
		expect(fixture).toHaveProperty('print');
		expect(fixture.print).toBeInstanceOf(Function);
	});
});
