import { resolve } from 'path';

const resolveAlias = require('../../dist').default;

export default {
	input: resolve(__dirname, './src/index.js'),
	output: {
		file: resolve(__dirname, './dist/bundle.js'),
		format: 'cjs'
	},
	plugins: [
		resolveAlias({
			aliases: {
				'@': resolve(__dirname, './src/'),
				'@lib': resolve(__dirname, './lib/')
			}
		})
	]
};
