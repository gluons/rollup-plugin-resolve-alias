export default function print(text) {
	if (process && process.stdout) {
		process.stdout.write(text);
	} else {
		console.log(text);
	}
}
