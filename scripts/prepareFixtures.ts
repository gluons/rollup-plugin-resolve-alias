import spawn from 'cross-spawn';
import { readdirSync, statSync } from 'fs';
import { join, resolve } from 'path';

const nodeBin = resolve(__dirname, '../node_modules/.bin/');
const rimrafBin = join(nodeBin, 'rimraf');
const rollupBin = join(nodeBin, 'rollup');

const fixturePath = resolve(__dirname, '../fixture/');
const fixtureDirs = readdirSync(fixturePath);

for (const fixtureDir of fixtureDirs) {
	const fullFixtureDir = resolve(fixturePath, fixtureDir);
	if (!statSync(fullFixtureDir).isDirectory()) {
		continue;
	}

	spawn.sync(rimrafBin, ['dist/*'], {
		cwd: fullFixtureDir,
		stdio: 'inherit',
		shell: true
	});
	spawn.sync(rollupBin, ['-c', 'rollup.config.ts'], {
		cwd: fullFixtureDir,
		stdio: 'inherit',
		shell: true
	});
}
