# rollup-plugin-resolve-alias
[![license](https://img.shields.io/badge/license-MIT-97CA00.svg?style=flat-square)](./LICENSE)
[![npm](https://img.shields.io/npm/v/rollup-plugin-resolve-alias.svg?style=flat-square)](https://www.npmjs.com/package/rollup-plugin-resolve-alias)
[![TSLint](https://img.shields.io/badge/TSLint-gluons-15757B.svg?style=flat-square)](https://github.com/gluons/tslint-config-gluons)
[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/gluons/rollup-plugin-resolve-alias/master.svg?style=flat-square)](https://gitlab.com/gluons/rollup-plugin-resolve-alias/pipelines)

Create aliases for [Rollup](https://rollupjs.org/) to import module easily.

## Installation

```bash
npm i -D rollup-plugin-resolve-alias
# or
yarn add -D rollup-plugin-resolve-alias
```

## Usage

**`rollup.config.js`:**
```js
import resolveAlias from 'rollup-plugin-resolve-alias';

export default {
	input: 'src/main.js',
	output: {
		file: 'bundle.js',
		format: 'cjs'
	},
	plugins: [
		resolveAlias({
			aliases: {
				// Your alias here
			}
		})
	]
};
```
