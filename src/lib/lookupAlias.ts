import containAlias from './containAlias';

/**
 * Lookup for an alias that match the given path.
 * (Use longest prefix match)
 *
 * @export
 * @param {Record<string, string>} aliases Aliases
 * @param {string} path Path
 * @returns {string}
 */
export default function lookupAlias(aliases: Record<string, string>, path: string): string {
	const matchedAliases = Object.keys(aliases).filter(alias => containAlias(alias, path));

	if (matchedAliases.length === 0) {
		return null;
	}

	/*
	 * Match the longest prefix.
	 */

	// Sort by length. Longer come first
	const sortedAliases = matchedAliases.sort((a, b) => b.length - a.length);

	return sortedAliases[0];
}
