/**
 * Check whether the given path contain given alias.
 *
 * @export
 * @param {string} alias Alias
 * @param {string} path Path
 * @returns {boolean}
 */
export default function containAlias(alias: string, path: string): boolean {
	return path.startsWith(alias);
}
