import normalizePath from '../utils/normalizePath';
import createAliasRegex from './createAliasRegex';

/**
 * Options for `resolveAlias`.
 *
 * @export
 * @interface ResolveAliasOptions
 */
export interface ResolveAliasOptions {
	/**
	 * Alias
	 *
	 * @type {string}
	 * @memberof ResolveAliasOptions
	 */
	alias: string;
	/**
	 * The real path for alias
	 *
	 * @type {string}
	 * @memberof ResolveAliasOptions
	 */
	realPath: string;
	/**
	 * The importee path
	 *
	 * @type {string}
	 * @memberof ResolveAliasOptions
	 */
	importeePath: string;
}

/**
 * Resolve an alias.
 *
 * @export
 * @param {ResolveAliasOptions} options Options
 * @returns {string}
 */
export default function resolveAlias(
	{
		alias,
		realPath,
		importeePath
	}: ResolveAliasOptions
): string {
	const aliasRegex = createAliasRegex(alias);
	const replacedPath = importeePath.replace(aliasRegex, realPath);
	let normalizedPath = normalizePath(replacedPath);

	return normalizedPath;
}
