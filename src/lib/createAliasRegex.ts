/**
 * Create alias Regex for using with path.
 *
 * @export
 * @param {string} alias Alias
 * @returns {RegExp}
 */
export default function createAliasRegex(alias: string): RegExp {
	const aliasRegex = new RegExp(`^(${alias})`);

	return aliasRegex;
}
