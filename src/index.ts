import { Plugin } from 'rollup';

import lookupAlias from './lib/lookupAlias';
import _resolveAlias from './lib/resolveAlias';

/**
 * `rollup-plugin-resolve-alias`'s options.
 *
 * @export
 * @interface Options
 */
export interface Options {
	/**
	 * Aliases
	 *
	 * @type {Record<string, string>}
	 * @memberof Options
	 */
	aliases: Record<string, string>;
}

/**
 * Rollup Plugin Resolve Alias
 *
 * @export
 * @param {Options} options Options
 * @returns {Plugin}
 */
export default function resolveAlias(options: Options): Plugin {
	const { aliases } = options;

	return {
		name: 'resolve-alias',
		resolveId(importee) {
			const targetAlias = lookupAlias(aliases, importee);

			if (!targetAlias) {
				return;
			}

			const realPath = aliases[targetAlias];
			const resolvedImporteePath = _resolveAlias({
				alias: targetAlias,
				realPath,
				importeePath: importee
			});

			return resolvedImporteePath;
		}
	};
}
