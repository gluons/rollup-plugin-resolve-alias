import { normalize } from 'path';

import isBacklashPath from './isBacklashPath';
import isMissingDot from './isMissingDot';

/**
 * Normalize path.
 *
 * @export
 * @param {string} path Path
 * @returns {string}
 */
export default function normalizePath(path: string): string {
	const normalizedPath = normalize(path);

	if (isMissingDot(normalizedPath)) {
		let dotPath = '.';
		dotPath += isBacklashPath(normalizedPath) ? '\\' : '/';

		return `${dotPath}${normalizedPath}`;
	}

	return normalizedPath;
}
