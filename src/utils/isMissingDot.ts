const validPathRegex = /^(\.|\.\.|\/|[A-Z]:).*/;

/**
 * Check whether the given path have no leading dot for local path.
 *
 * @export
 * @param {string} path
 * @returns {boolean}
 */
export default function isMissingDot(path: string): boolean {
	return !validPathRegex.test(path);
}
