/**
 * Check whether the given path is Windows (backslash) path.
 *
 * @export
 * @param {string} path Path
 * @returns {boolean}
 */
export default function isBacklashPath(path: string): boolean {
	return path.includes('\\');
}
